## Exercice react Typescript


Cet exercice parcours quelques notions de bases de React : boucler sur des composants, travailler en asynchrone, mettre en place un routage front...

Le but de l'exercice est de recréer en partie la page ["Qui sommes nous"](https://www.mereo.fr/qui-sommes-nous/notre-equipe/) de mereo.fr avec les membres de notre équipe.

La structure du projet est la même que celle sur laquelle nous travaillons, simplifiée.

### Exercices
1. Les données pour récupérer la liste des membres sont accessibles dans un service asynchrone. Il faut la récupérer *dans App.tsx* pour l'envoyer à la page *Team*.
2. Dans */src/components/TeamMembersList.tsx*, boucler sur la liste pour afficher tous les membres de l'équipe reçus en props.
3. Ajouter au composant *TeamMember.tsx* un boutton permettant de changer de route pour matcher la route *member/:memberId*. Nous utilisons react-router 6.0 avec la fonction *useNavigate()*.
4. Récupérer l'id du membre dans la page *TeamMemberPage.tsx* depuis l'url et appeler le service assynchrone en complétant la méthode *getTeamMemberById()*


### Installation ###

- npm install

- npm run start