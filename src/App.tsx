import React from "react";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import Team from './pages/Team';
import TeamMemberPage from "./pages/TeamMemberPage";

function App() {

  // 1. Récupérer la liste des membres depuis le TeamService
  
  return (
    <BrowserRouter>
      <div style={styles.app}>
        <header style={styles.appHeader}>
          <Routes>
            <Route path="/" element={<Team members={members} />} />
            <Route path="member/:memberId" element={<TeamMemberPage />} />
          </Routes>
        </header>
      </div>
    </BrowserRouter>
  );
}

const styles = {
  app: {
    textAlign: "center" as const,
  },
  appHeader: {
    backgroundColor: "#282c34",
    minHeight: "100vh",
    display: "flex",
    flexDirection: "column" as const,
    alignItems: "center",
    fontSize: "calc(10px + 2vmin)",
    color: "white"
  }
}

export default App;
