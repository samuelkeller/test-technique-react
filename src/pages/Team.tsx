import React from "react";
import TeamMember from "../components/TeamMember";
import TeamMembersList from "../components/TeamMembersList";
import TeamService from "../services/TeamService"

interface TeamProps {
    members: TeamMember[]
}

function Team({ members }: TeamProps) {
    return (
        <div style={styles.teamPage}>
            <div style={styles.headerBackground}>
                <p> Notre équipe de geeks</p>
            </div>
            <div style={styles.teamMemberList}>
                <TeamMembersList members={members}/>
            </div>
        </div>
    )
}

const styles = {
    teamPage: {
        width: "100%"
    },
    headerBackground: {
        display: "flex",
        flexDirection: "column" as const,
        height: "20vh",
        backgroundImage: "linear-gradient(90deg, #6DB5BB 0%, #66DDBBAA 94%)",
        opacity: 0.81
    },
    teamMemberList: {
        padding: "30px",
        display: "flex",
        flexDirection: "row" as const,
    }
};

export default Team;