import React, { useState } from "react";
import TeamMember from "../components/TeamMember";

function TeamMemberPage() {
    const [member, setMember] = useState<TeamMember | null>(null);

    // Récupérer l'id du membre.
    // Utiliser le service pour récupérer ses infos.

    return (
        member && (
            <>
                <h5>Bienvenue sur la page de {member.name} !</h5>
                <TeamMember member={member} />
            </>
        )
    )
}

export default TeamMemberPage;