interface TeamMember {
      id: number,
      name: string,
      description: string,
      image: path  
}