import TeamMember from "../components/TeamMember";

const ALL_TEAM_DATA = [
    {
        id: 1,
        name: "Anas Amara",
        description: "Expat ? eh oh !",
        image: "https://media.licdn.com/dms/image/D4E03AQGoeXR3CURZYg/profile-displayphoto-shrink_200_200/0/1705384976931?e=1724889600&v=beta&t=IBT-cunAA7YiHJFKjVNoArBX6IkBhfpM8ZEpNTwnJ4c"
    }, {
        id: 2,
        name: "Coraline Sotos",
        description: "Dj résident",
        image: "https://media.licdn.com/dms/image/C4D35AQGy9nZ20GI9ew/profile-framedphoto-shrink_400_400/0/1616747647676?e=1720432800&v=beta&t=AbGUIlaC3TQtSDUPn0TpyhYOg2QnpcGwP6o3rl-lsd0"
    },  {
        id: 3,
        name: "Lola Dubosc",
        description: "Lolacratie",
        image: "https://www.mereo.fr/wp-content/uploads/2020/12/LDU_1.png"
    }, {
        id: 4,
        name: "Mamadou Kanté",
        description: "Le Bouddha du code",
        image: "https://www.mereo.fr/wp-content/uploads/2020/12/MKA_1-300x222.png"
    }, {
        id: 5,
        name: "Samuel Keller",
        description: "La police",
        image: "https://lh3.googleusercontent.com/a/ACg8ocIlF0YKd6sWubktZcLuwB1YYN8ocmETlndaeZv8pBTxoLZ74SE=s83-c-mo"
    }
];

export default class TeamService  {
    /**
     * Récupères la liste des membres.
     */
    getTeamMembersList(): Promise<TeamMember[]> {
        return new Promise((resolve, reject) => {
            resolve(ALL_TEAM_DATA)
        })
    }
    /**
     * @todo Euh le service marche pas.
     */
    getTeamMemberById(): Promise<TeamMember | null> {
        return new Promise((resolve, reject) => {
            resolve(null)
        })
    }
}
