import React, { CSSProperties } from "react";

interface TeamMemberProps {
    member: TeamMember
}

function TeamMember({ member }: TeamMemberProps) {

    return (
        <div style={styles.teamMemberContainer}>
            <img src={member.image} alt={member.name} style={styles.teamMemberImg} />
            <h3>{member.name}</h3>
            <p>{member.description}</p>
            {/* Bouton pour changer de membre */}
        </div>
    )
}


const styles = {
    teamMemberContainer: {
        maxWidth: "25%",
        maxHeight: "25%",
        backgroundColor: "white",
        margin: "10px",
        color: "grey"
    },
    teamMemberImg: {
        maxWidth: "80%",
        maxHeight: "80%"
    }
};

export default TeamMember;